﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logic.DTO.Request
{
    public class CategoryRequestDto
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
