﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logic.DTO.Request
{
    public class PersonRequestDto
    {
        public string FirstName { get; set; }

        public string SecondName { get; set; }

        public string LastName { get; set; }

        public DateTime BirthDate { get; set; }

        public string Address { get; set; }
    }
}
