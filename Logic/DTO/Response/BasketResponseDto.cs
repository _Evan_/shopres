﻿using Data.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Logic.DTO.Response
{
    public class BasketResponseDto
    {
        public List<Product> Products { get; set; }
    }
}
