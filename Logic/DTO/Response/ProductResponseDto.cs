﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Logic.DTO.Response
{
    public class ProductResponseDto
    {
        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        public int MyProperty { get; set; }
    }
}
