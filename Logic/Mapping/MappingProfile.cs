﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using Data.Entity;
using Logic.DTO.Request;
using Logic.DTO.Response;

namespace Logic.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Product, ProductResponseDto>().ReverseMap();
            CreateMap<Product, ProductRequestDto>().ReverseMap();

            CreateMap<Category, CategoryResponseDto>().ReverseMap();
            CreateMap<Category, CategoryRequestDto>().ReverseMap();

            CreateMap<Person, PersonResponseDto>().ReverseMap();
            CreateMap<Person, PersonRequestDto>().ReverseMap();

            CreateMap<Basket, BasketResponseDto>().ReverseMap();
            CreateMap<Basket, BasketRequestDto>().ReverseMap();
        }
    }
}
