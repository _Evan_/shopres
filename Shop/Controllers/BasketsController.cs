﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data.IRepositories;
using Microsoft.AspNetCore.Mvc;

namespace Shop.Controllers
{
    [Route("api/[controller]")]
    public class BasketsController : Controller
    {
        private readonly IBasketRepository repository;

        public BasketsController(IBasketRepository repository)
        {
            this.repository = repository;
        }

        // GET api/products
        [HttpGet]
        public IActionResult Get()
        {
            return Ok();
        }

        // GET api/basket/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(repository.GetAllProducts(id).ToList());
        }

        // POST api/products
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]int basketId, int productId)
        {
            var entity = await repository.AddProductAsync(basketId,productId);

            return CreatedAtRoute(nameof(Get),entity);
        }

        //// PUT api/products/5
        //[HttpPut("{id}")]
        //public async Task<IActionResult> Put(int id, [FromBody]Product model)
        //{
        //    if (model == null || model.ProductId != id)
        //    {
        //        return BadRequest();
        //    }

        //    var entity = await repository.FindAsync(id);
        //    if (entity == null)
        //    {
        //        return NotFound();
        //    }

        //    await repository.UpdateAsync(model);
        //    return new NoContentResult();
        //}

        //// DELETE api/products/5
        //[HttpDelete("{id}")]
        //public async Task<IActionResult> Delete(int id)
        //{
        //    try
        //    {
        //        await repository.RemoveAsync(id);
        //    }
        //    catch (InvalidOperationException e)
        //    {
        //        Console.WriteLine(e);
        //        return BadRequest("No Product found with id " + id);
        //    }

        //    return new NoContentResult();
        //}
    }
}