﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data.Entity;
using Data.IRepositories;
using Logic.DTO.Request;
using Microsoft.AspNetCore.Mvc;

namespace Shop.Controllers
{

    [Route("api/[controller]")]
    public class ProductsController : Controller
    {
        private readonly IProductRepository repository;

        public ProductsController(IProductRepository repository)
        {
            this.repository = repository;
        }

        // GET api/products
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(repository.GetAll().ToList());
        }

        // GET api/products/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var entity = await repository.FindAsync(id);
            if (entity == null)
                return NotFound();
            return Ok(entity);
        }

        // POST api/products
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]ProductRequestDto productDto)
        {
            if (productDto == null)
                return BadRequest("Please provide a Product.");

            Product product = new Product();
            product.Name = productDto.Name;
            product.Description = productDto.Description;
            product.Price = productDto.Price;
            var entity = await repository.AddAsync(product);

            return Ok();
        }

        // PUT api/products/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody]Product model)
        {
            if (model == null || model.ProductId != id)
            {
                return BadRequest();
            }

            var entity = await repository.FindAsync(id);
            if (entity == null)
            {
                return NotFound();
            }

            await repository.UpdateAsync(model);
            return new NoContentResult();
        }

        // DELETE api/products/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await repository.RemoveAsync(id);
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine(e);
                return BadRequest("No Product found with id " + id);
            }

            return new NoContentResult();
        }
    }
}