﻿using Data.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Data
{
    public class ShopContext : DbContext
    {
        public ShopContext(DbContextOptions options) : base(options)
        {

        }

        public virtual DbSet<Basket> Baskets { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Person> People { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
          //  modelBuilder.ApplyConfiguration(new category configuration());
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<CategoryProduct>()
                .HasKey(t => new { t.CategoryId, t.ProductId });

            modelBuilder.Entity<CategoryProduct>()
                .HasOne(p => p.Category)
                .WithMany(x => x.CategoryProducts)
                .HasForeignKey(y => y.CategoryId);

            modelBuilder.Entity<CategoryProduct>()
               .HasOne(p => p.Product)
               .WithMany(x => x.CategoryProducts)
               .HasForeignKey(y => y.ProductId);

            modelBuilder.Entity<BasketProduct>()
            .HasKey(t => new { t.BasketId, t.ProductId });

            modelBuilder.Entity<BasketProduct>()
                .HasOne(p => p.Basket)
                .WithMany(x => x.BasketProducts)
                .HasForeignKey(y => y.BasketId);

            modelBuilder.Entity<BasketProduct>()
               .HasOne(p => p.Product)
               .WithMany(x => x.BasketProducts)
               .HasForeignKey(y => y.ProductId);

        }
    }
}
