﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Data.Entity
{
    [Table("BasketsProducts")]
    public class BasketProduct
    {
        public int BasketId { get; set; }
        public Basket Basket { get; set; }

        public int ProductId { get; set; }
        public Product Product { get; set; }
    }
}
