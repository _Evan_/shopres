﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Data.Entity
{
    [Table("Products")]
    public class Product
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProductId { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }

        public string Description { get; set; }

        public ICollection<CategoryProduct> CategoryProducts { get; set; }

        public ICollection<BasketProduct> BasketProducts { get; set; }

        public Product()
        {
            CategoryProducts = new Collection<CategoryProduct>();
            BasketProducts = new Collection<BasketProduct>();
        }
    }
}
