﻿using Data.Entity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Data.Entity
{
    [Table("Baskets")]
    public class Basket
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BasketId { get; set; }

        public Person Person { get; set; }

        internal object Include()
        {
            throw new NotImplementedException();
        }

        public ICollection<BasketProduct> BasketProducts { get; set; }

        public Basket()
        {
            BasketProducts = new Collection<BasketProduct>();
        }
    }
}
