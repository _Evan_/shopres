﻿using Data.Entity;
using Data.IRepositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class BasketRepository : IBasketRepository
    {
        private readonly ShopContext _context;

        public BasketRepository(ShopContext context)
        {
            _context = context;
        }
        public IEnumerable<Product> GetAllProducts(int basketId)
        {
            var products = _context.Baskets.Find(basketId).BasketProducts.Select(x => x.Product);

           return products;
        }
        public async Task<Basket> AddProductAsync(int basketId, int productId)
        {
            var basket = _context.Baskets.Find(basketId);

            basket.BasketProducts.Add( new BasketProduct() {
                BasketId = basketId, ProductId=productId }
            );

            return basket;
        }
    }
}
