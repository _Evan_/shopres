﻿using Data.Entity;
using Data.IRepositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{

    public class CategoryRepository : ICategoryRepository
    {
        private readonly ShopContext _context;

        public CategoryRepository(ShopContext context)
        {
            _context = context;
        }

        public async Task<Category> AddAsync(Category item)
        {
            _context.Categories.Add(item);
            await _context.SaveChangesAsync();

            return item;
        }

        public IEnumerable<Category> GetAll()
        {
            return _context.Categories;
        }

        public async Task<Category> FindAsync(int id)
        {
            return await _context.Categories.FirstOrDefaultAsync(p => p.CategoryId == id);
        }

        public async Task RemoveAsync(int id)
        {
            var entity = await _context.Categories.SingleOrDefaultAsync(p => p.CategoryId == id);
            if (entity == null)
                throw new InvalidOperationException("No category found matching id " + id);

            _context.Categories.Remove(entity);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(Category item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            _context.Categories.Update(item);
            await _context.SaveChangesAsync();
        }
    }
}
