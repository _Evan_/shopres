﻿using Data.Entity;
using Data.IRepositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private readonly ShopContext _context;

        public ProductRepository(ShopContext context)
        {
            _context = context;
        }

        public async Task<Product> AddAsync(Product item)
        {
            _context.Products.Add(item);
            await _context.SaveChangesAsync();

            return item;
        }

        public IEnumerable<Product> GetAll()
        {
            return _context.Products;
        }

        public async Task<Product> FindAsync(int id)
        {
            return await _context.Products.FirstOrDefaultAsync(p => p.ProductId == id);
        }

        public async Task RemoveAsync(int id)
        {
            var entity = await _context.Products.SingleOrDefaultAsync(p => p.ProductId == id);
            if (entity == null)
                throw new InvalidOperationException("No product found matching id " + id);

            _context.Products.Remove(entity);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(Product item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            _context.Products.Update(item);
            await _context.SaveChangesAsync();
        }
    }
}
