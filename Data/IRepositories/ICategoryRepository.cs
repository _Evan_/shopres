﻿using Data.Entity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Data.IRepositories
{
    public interface ICategoryRepository
    {
        Task<Category> AddAsync(Category item);
        IEnumerable<Category> GetAll();
        Task<Category> FindAsync(int id);
        Task RemoveAsync(int id);
        Task UpdateAsync(Category item);
    }
}
