﻿using Data.Entity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Data.IRepositories
{
    public interface IBasketRepository
    {
       Task<Basket> AddProductAsync(int basketId,int productId);
       // Task<Basket> RemoveProductAsync(int basketId, int productId);

       IEnumerable<Product> GetAllProducts(int basketId);
    }
}
