﻿using Data.Entity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Data.IRepositories
{
    public interface IProductRepository
    {
        Task<Product> AddAsync(Product item);
        IEnumerable<Product> GetAll();
        Task<Product> FindAsync(int id);
        Task RemoveAsync(int id);
        Task UpdateAsync(Product item);
    }
}
