﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class NewOptions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BasketProduct_Baskets_BasketId",
                table: "BasketProduct");

            migrationBuilder.DropForeignKey(
                name: "FK_BasketProduct_Products_ProductId",
                table: "BasketProduct");

            migrationBuilder.DropForeignKey(
                name: "FK_CategoryProduct_Categories_CategoryId",
                table: "CategoryProduct");

            migrationBuilder.DropForeignKey(
                name: "FK_CategoryProduct_Products_ProductId",
                table: "CategoryProduct");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CategoryProduct",
                table: "CategoryProduct");

            migrationBuilder.DropPrimaryKey(
                name: "PK_BasketProduct",
                table: "BasketProduct");

            migrationBuilder.RenameTable(
                name: "CategoryProduct",
                newName: "CategoriesProducts");

            migrationBuilder.RenameTable(
                name: "BasketProduct",
                newName: "BasketsProducts");

            migrationBuilder.RenameIndex(
                name: "IX_CategoryProduct_ProductId",
                table: "CategoriesProducts",
                newName: "IX_CategoriesProducts_ProductId");

            migrationBuilder.RenameIndex(
                name: "IX_BasketProduct_ProductId",
                table: "BasketsProducts",
                newName: "IX_BasketsProducts_ProductId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CategoriesProducts",
                table: "CategoriesProducts",
                columns: new[] { "CategoryId", "ProductId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_BasketsProducts",
                table: "BasketsProducts",
                columns: new[] { "BasketId", "ProductId" });

            migrationBuilder.AddForeignKey(
                name: "FK_BasketsProducts_Baskets_BasketId",
                table: "BasketsProducts",
                column: "BasketId",
                principalTable: "Baskets",
                principalColumn: "BasketId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BasketsProducts_Products_ProductId",
                table: "BasketsProducts",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "ProductId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CategoriesProducts_Categories_CategoryId",
                table: "CategoriesProducts",
                column: "CategoryId",
                principalTable: "Categories",
                principalColumn: "CategoryId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CategoriesProducts_Products_ProductId",
                table: "CategoriesProducts",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "ProductId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BasketsProducts_Baskets_BasketId",
                table: "BasketsProducts");

            migrationBuilder.DropForeignKey(
                name: "FK_BasketsProducts_Products_ProductId",
                table: "BasketsProducts");

            migrationBuilder.DropForeignKey(
                name: "FK_CategoriesProducts_Categories_CategoryId",
                table: "CategoriesProducts");

            migrationBuilder.DropForeignKey(
                name: "FK_CategoriesProducts_Products_ProductId",
                table: "CategoriesProducts");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CategoriesProducts",
                table: "CategoriesProducts");

            migrationBuilder.DropPrimaryKey(
                name: "PK_BasketsProducts",
                table: "BasketsProducts");

            migrationBuilder.RenameTable(
                name: "CategoriesProducts",
                newName: "CategoryProduct");

            migrationBuilder.RenameTable(
                name: "BasketsProducts",
                newName: "BasketProduct");

            migrationBuilder.RenameIndex(
                name: "IX_CategoriesProducts_ProductId",
                table: "CategoryProduct",
                newName: "IX_CategoryProduct_ProductId");

            migrationBuilder.RenameIndex(
                name: "IX_BasketsProducts_ProductId",
                table: "BasketProduct",
                newName: "IX_BasketProduct_ProductId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CategoryProduct",
                table: "CategoryProduct",
                columns: new[] { "CategoryId", "ProductId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_BasketProduct",
                table: "BasketProduct",
                columns: new[] { "BasketId", "ProductId" });

            migrationBuilder.AddForeignKey(
                name: "FK_BasketProduct_Baskets_BasketId",
                table: "BasketProduct",
                column: "BasketId",
                principalTable: "Baskets",
                principalColumn: "BasketId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BasketProduct_Products_ProductId",
                table: "BasketProduct",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "ProductId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CategoryProduct_Categories_CategoryId",
                table: "CategoryProduct",
                column: "CategoryId",
                principalTable: "Categories",
                principalColumn: "CategoryId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CategoryProduct_Products_ProductId",
                table: "CategoryProduct",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "ProductId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
